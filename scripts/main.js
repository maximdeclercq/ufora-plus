/* Common objects that provide an easy interface to e.g. cookies */

// An object providing access to cookies
const cookies = {
    // Get a cookie value
    get(name) {
        // Search all cookies for a matching value
        let cookies = decodeURIComponent(document.cookie).split(";")
        cookies.forEach(function(value) {
            if (value.startsWith(`${name}=`)) {
                return value.substring(`${name}=`.length)
            }
        })
        return null
    },
    // Set a cookie value (do not expire by default)
    set(name, value, expires=new Date(3000, 1, 1)) {
        document.cookie = `${name}=${value};expires=${expires.toUTCString()};path=/`
    },
    // Delete a cookie
    unset(name) {
        // Set the expire date to a passed date
        set(name, "", new Date(1000, 1, 1))
    }
}

/* Hack to insert the custom style into iframes */
const darkStylePath = "styles/dark.css"

window.onload = function() {
    browser.tabs.insertCSS({ allFrames: true, file: darkStylePath })
}
