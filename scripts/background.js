const title = "Ufora+"
const iconPath = "icons/ufora-plus-192.png"
const mainScriptPath = "scripts/main.js"
const darkStylePath = "styles/dark.css"
const hostname = "ufora.ugent.be" 

// When first loaded, register the extension on all tabs
browser.tabs.query({}).then((tabs) => {
  for (let tab of tabs) {
    registerExtension(tab)
  }
})

// Each time a tab is updated, reregister the extension
browser.tabs.onUpdated.addListener((id, changeInfo, tab) => {
    registerExtension(tab)
})

// Register the on the specified tab
function registerExtension(tab) {
    // Do not register the extension on non-Ufora pages
    if (new URL(tab.url).hostname != hostname) return

    // Add page action to the address bar
    browser.pageAction.setTitle({tabId: tab.id, title})
    browser.pageAction.setIcon({tabId: tab.id, path: iconPath})
    browser.pageAction.show(tab.id)

    // Inject the extension script
    browser.tabs.executeScript(tab.id, { file: mainScriptPath })

    // Inject the dark mode style
    let darkMode = true
    if (darkMode) {
        browser.tabs.insertCSS(tab.id, { allFrames: true, cssOrigin: "user", file: darkStylePath })
    }
    else {
        browser.tabs.removeCSS(tab.id, { file: darkStylePath })
    }

    // Insert into iframes
    tab.window.onload = function() {
        browser.tabs.insertCSS(tab.id, { allFrames: true, cssOrigin: "user", file: darkStylePath })
    }
}
