DISCLAIMER: Ufora and the Ufora logo are property of UGent University.

# Ufora+

A Firefox extension that enhances Ufora with several features.

### Setup
```shell script
# Ubuntu
$ sudo apt install web-ext
# Arch
$ sudo pacman -Sy web-ext
```

### Testing
```shell script
$ web-ext run --target=firefox-desktop
```

### Support
<a href="https://www.buymeacoffee.com/maximdeclercq" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" alt="Buy Me A Coffee" style="height: 60px !important;width: 217px !important;" ></a>
